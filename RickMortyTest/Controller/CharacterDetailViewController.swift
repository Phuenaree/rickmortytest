//
//  CharacterDetailViewController.swift
//  RickMortyTest
//
//  Created by Kitbodee Phuenaree on 15/1/2563 BE.
//  Copyright © 2563 Kitbodee Phuenaree. All rights reserved.
//

import UIKit
import SDWebImage

class CharacterDetailViewController: UIViewController {

    @IBOutlet weak var characterImageView: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var speciesLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var originLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    var character:Character?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = character?.name
        characterImageView.sd_setImage(with: URL(string: (character?.image)!), completed: nil)
        characterImageView.layer.cornerRadius = 20
        characterImageView.clipsToBounds = true
        statusLabel.text = character?.status
        speciesLabel.text = character?.species
        genderLabel.text = character?.gender
        typeLabel.text = character?.type == "" ? "N/A" : character?.type
        originLabel.text = character?.origin.name
        locationLabel.text = character?.location.name
    }
    
}
