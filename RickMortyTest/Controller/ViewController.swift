//
//  ViewController.swift
//  RickMortyTest
//
//  Created by Kitbodee Phuenaree on 13/1/2563 BE.
//  Copyright © 2563 Kitbodee Phuenaree. All rights reserved.
//

import UIKit
import SDWebImage






class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {

    @IBOutlet var tableView: UITableView!
    var characters = [Character]()
    var nextPageData:String?
    var prevPageData:String?
    var pagesData: Int?
    var currentPage: Int = 1 {
        didSet {
            // if currentPage value is change set new value to currentPageLabel.text
            currentPageLabel.text = String(currentPage)
        }
    }
    var characterSelected = 0
    var url = "https://rickandmortyapi.com/api/character/"
    @IBOutlet weak var currentPageLabel: UILabel!
    
        
    override func viewDidLoad() {
        super.viewDidLoad()
        getRickAndMortyData()
        
        
        self.tableView.dataSource = self
        self.tableView.delegate = self

    }

    func getRickAndMortyData() {
        
        
        //create instance of the url
        //let url = "https://rickandmortyapi.com/api/character/?page=\(currentPage)"
        
        //construct the url, use guard to avoid nonoptional
        guard let urlObj = URL(string: url) else
        { return }
        
        //fetch data
        URLSession.shared.dataTask(with: urlObj) {(data, response, error) in
            
            //to avoid non optional in JSONDecoder
            guard let data = data else { return }
            
            do {
                //decode object
                let rickAndMortyData = try JSONDecoder().decode(CharacterResponse.self, from: data)
                self.characters = rickAndMortyData.results
                self.pagesData = rickAndMortyData.info.pages
                self.nextPageData = rickAndMortyData.info.next
                self.prevPageData = rickAndMortyData.info.prev
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    print(self.currentPage)
                }
                
            } catch {
                print(error)
                
            }
            
            }.resume()
        
    }
    

    
    

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return characters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "rickandmortyCell") as? CharacterTableViewCell else { return UITableViewCell() }
        //getLabel
        cell.nameLabel.text = characters[indexPath.row].name
        cell.statusLabel.text = "Status: " + characters[indexPath.row].status
        //getPicture
        cell.imgView.sd_setImage(with: URL(string: characters[indexPath.row].image), completed: nil)
        
        
        
        return cell
    }
    
    
    // Show Character Detail View Controller
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        characterSelected = indexPath.row
        performSegue(withIdentifier: "showDetail", sender: self)
        
    }
    // *** send indexPath.row ***
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            // Get the new view controller using segue.destination.
            // Pass the selected object to the new view controller.
            if (segue.identifier == "showDetail") {
                let showDetailViewController: CharacterDetailViewController = segue.destination as! CharacterDetailViewController
                 showDetailViewController.character = characters[characterSelected]
            }
        }
        
        
    @IBAction func backButtonPress(_ sender: Any) {
        currentPage -= 1
        // Check if prevPageData have data = Go to previous page
        guard let prevPageData = prevPageData , prevPageData != "" else {
                return currentPage = 1
            }
            // set new url to getRickAndMortyData() for go to previous page
            url = prevPageData
            getRickAndMortyData()
        }
    
    
    @IBAction func nextButtonPress(_ sender: Any) {
        currentPage += 1
        // Check if nextPageData have data = Go to next page
        guard let nextPageData = nextPageData , nextPageData != "" else {
            return currentPage = pagesData!
        }
        // set new url to getRickAndMortyData() for go to next page
        url = nextPageData
        getRickAndMortyData()
    }
    
}
