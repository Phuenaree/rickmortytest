//
//  Location.swift
//  RickMortyTest
//
//  Created by Kitbodee Phuenaree on 27/1/2563 BE.
//  Copyright © 2563 Kitbodee Phuenaree. All rights reserved.
//

import Foundation

struct Location: Codable {
    let id: Int?
    let name: String
    let type: String?
    let dimension: String?
    let residents: [String]?
    let url: String
    let created: String?
}
