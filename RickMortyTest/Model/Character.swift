//
//  Character.swift
//  RickMortyTest
//
//  Created by Kitbodee Phuenaree on 15/1/2563 BE.
//  Copyright © 2563 Kitbodee Phuenaree. All rights reserved.
//

import Foundation

struct CharacterResponse: Codable {
    let info: Info
    let results: [Character]
}

struct Info: Codable {
    let count: Int
    let pages: Int
    let next: String
    let prev: String
}

struct Character: Codable {
    let name: String
    let status: String
    let species: String
    let type: String
    let gender: String
    let image: String
    let origin: Location
    let location: Location
}




